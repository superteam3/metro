package org.superteam.metro.util;

import org.superteam.metro.builder.factory.AbstractFactory;
import org.superteam.metro.builder.factory.StationSource;
import org.superteam.metro.builder.factory.jsonimpl.JsonFactory;
import org.superteam.metro.model.Station;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shubbush on 14/05/16.
 */
public class StationsUtil {

    private static StationsUtil stationsUtil = new StationsUtil();

    private AbstractFactory factory;
    private StationSource stationSource;

    private List<Station> stationList;
    private Map<String, Station> stationMap;

    public static StationsUtil getInstance() {
        return stationsUtil;
    }

    private StationsUtil() {
        factory = new JsonFactory();
        stationSource = factory.createStantionSource();
        stationList = stationSource.getStationList();
        createStationMap();
    }

    private void createStationMap() {
        stationMap = new HashMap<>();
        for(Station station : stationList) {
            stationMap.put(station.getId(), station);
        }
    }


    public List<Station> getStationList() {
        return stationList;
    }

    public void setStationList(List<Station> stationList) {
        this.stationList = stationList;
    }

    public Station getStationById(String stationId) {
        return stationMap.get(stationId);
    }
}
