package org.superteam.metro.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/**
 * Created by shubbush on 15/05/16.
 */
public class AnimationFactory {


    public TranslateAnimation getDownToUpShowAnimation(final View object) {
        TranslateAnimation animate = new TranslateAnimation(0,0,object.getHeight(),0);
        animate.setDuration(250);
        animate.setFillAfter(true);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                object.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return animate;
    }

    public TranslateAnimation getUpToDownHideAnimation(final View object) {
        TranslateAnimation animate = new TranslateAnimation(0,0,0,object.getHeight());
        animate.setDuration(250);
        animate.setFillAfter(true);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                object.setAnimation(null);
                object.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return animate;
    }

}
