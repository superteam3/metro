package org.superteam.metro.util;

import org.superteam.metro.builder.factory.AbstractFactory;
import org.superteam.metro.builder.factory.StageSource;
import org.superteam.metro.builder.factory.jsonimpl.JsonFactory;
import org.superteam.metro.model.Peregon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shubbush on 22/05/16.
 */
public class TransferUtil {

    private static TransferUtil transferUtil = new TransferUtil();

    private AbstractFactory factory;
    private StageSource stageSource;


    private List<Peregon> transferList;
    private Map<String, Peregon> transferMap = new HashMap<>();

    private TransferUtil() {
        factory = new JsonFactory();
        stageSource = factory.createStageSource();
        transferList = stageSource.getStageList();
        for(Peregon peregon : transferList) {
            transferMap.put(peregon.getId(), peregon);
        }
    }

    public Peregon getTransferById(String id) {
        return transferMap.get(id);
    }

    public static TransferUtil getInstance() {
        return transferUtil;
    }

}
