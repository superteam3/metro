package org.superteam.metro.jsinterface;

import android.graphics.Point;
import android.webkit.JavascriptInterface;

import org.superteam.metro.MainActivity;
import org.superteam.metro.util.StationsUtil;

import java.util.Date;


/**
 * Created by shubbush on 28/04/16.
 */
public class MapInterface {

    private MainActivity mainActivity;

    @JavascriptInterface
    public void regionActivity(String stationId) {
        mainActivity.onSelectStation(stationId);
//        mainActivity.showSelectionLayout(new Point(x,y));
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }
}
