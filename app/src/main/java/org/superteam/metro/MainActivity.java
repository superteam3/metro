package org.superteam.metro;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.superteam.metro.builder.Resolver;
import org.superteam.metro.jsinterface.MapInterface;
import org.superteam.metro.model.Station;
import org.superteam.metro.util.AnimationFactory;
import org.superteam.metro.util.StationsUtil;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    final int sdk = android.os.Build.VERSION.SDK_INT;

    //Utils
    private AnimationFactory animationFactory = new AnimationFactory();
    private MapInterface mapInterface;
    private StationsUtil stationsUtil = StationsUtil.getInstance();
    private Resolver resolver = new Resolver();

    //Views
    private WebView webView;
    private LinearLayout selectionLayoyt;
    private Point lastTouch;
    private GestureDetector gestureDetector;
    boolean zoomed = false;
    private ListView stationsList;
    private StationListAdapter stationsAdapter;
    private LinearLayout stationsListLayout;
    private TextView startRoute;
    private TextView endRoute;
    private ImageButton startRouteButton;
    private ImageButton endRouteButton;
    private ImageButton startRouteButtonDelete;
    private ImageButton endRouteButtonDelete;
    private EditText searchStation;
    private TextView chooserTextView;
    private LinearLayout chooserCircle;
    private LinearLayout startRouteCircle;
    private LinearLayout endRouteCircle;
    private LinearLayout timingLinearLayout;
    private TextView timingText;
    private TextView arriveText;

    //Models
    private Station startStation;
    private Station endStation;
    private Station selectedStation;

    //Animations
    private TranslateAnimation showStationList;
    private TranslateAnimation hideStationList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gestureDetector = new GestureDetector(getBaseContext(), new GestureListener());
        chooserCircle = (LinearLayout) findViewById(R.id.chooserCircle);
        startRouteCircle = (LinearLayout) findViewById(R.id.startRouteCircle);
        endRouteCircle = (LinearLayout) findViewById(R.id.endRouteCircle);
        timingLinearLayout = (LinearLayout) findViewById(R.id.timingLinearLayout);
        timingText = (TextView) findViewById(R.id.timingText);
        arriveText = (TextView) findViewById(R.id.arriveText);
        initMapView();
        initStationsView();
        initStationsList();

    }

    private void initMapView() {
        selectionLayoyt = (LinearLayout) findViewById(R.id.selectionLayoyt);
        mapInterface = new MapInterface();
        mapInterface.setMainActivity(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        lastTouch = new Point((int)event.getX(),(int) event.getY());
        return false;
    }

    private void initStationsView() {
        webView = (WebView) findViewById(R.id.webView2);
        chooserTextView = (TextView) findViewById(R.id.chooserTextView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setBackgroundColor(Color.TRANSPARENT);
        //Для увеличения по дабл тапу
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //Скрытие градиентных областей при оверскролле
        webView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        webView.addJavascriptInterface(mapInterface, "Android");
        //Проверка платности
        webView.loadUrl("file:///android_asset/stations.html");
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                return true;
            }
        });
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                if(selectionLayoyt.getVisibility() == View.VISIBLE) {
                    hideSelectionLayout();
//                    webView.loadUrl("javascript:removeOverlay()");
                }
                lastTouch = new Point((int)event.getX(),(int) event.getY());
                return false;
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("Metro", cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId() );
                return true;
            }
        });
    }

    private void initStationsList() {

        stationsList = (ListView) findViewById(R.id.stationsList);
        stationsListLayout = (LinearLayout) findViewById(R.id.stationsListLayout);
        startRoute = (TextView) findViewById(R.id.startRoute);
        endRoute = (TextView) findViewById(R.id.endRoute);
        startRouteButton  = (ImageButton) findViewById(R.id.startRoutePic);
        endRouteButton  = (ImageButton) findViewById(R.id.endRoutePic);
        startRouteButtonDelete  = (ImageButton) findViewById(R.id.startRoutePicDelete);
        endRouteButtonDelete = (ImageButton) findViewById(R.id.endRoutePicDelete);

        searchStation = (EditText) findViewById(R.id.searchStation);

        stationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedStation = stationsAdapter.getItem(position);
                if(startRoute.isActivated())
                    selectStartStation(view);
                if(endRoute.isActivated())
                    selectEndStation(view);
                hideStationList(view);
            }
        });

        stationsAdapter = new StationListAdapter(this, R.layout.station_list_item, stationsUtil.getStationList());

        stationsList.setAdapter(stationsAdapter);
        searchStation.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                stationsAdapter.getFilter().filter(arg0);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {}

            @Override
            public void afterTextChanged(Editable arg0) {}
        });

    }

    public void showSelectionLayout() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                Point point = new Point(lastTouch.x, lastTouch.y);

                int width = size.x;
                int height = size.y;

                if(lastTouch.x + selectionLayoyt.getWidth() > width) {
                    point.x = width - selectionLayoyt.getWidth();
                }
                if(lastTouch.y + selectionLayoyt.getHeight() > height) {
                    point.y = height - selectionLayoyt.getHeight();
                }

                selectionLayoyt.setX(point.x);
                selectionLayoyt.setY(point.y);

                selectionLayoyt.setVisibility(View.VISIBLE);
                chooserTextView.setText(selectedStation.getName());
                GradientDrawable gradientDrawable = (GradientDrawable) chooserCircle.getBackground();
                gradientDrawable.setColor(Color.parseColor(selectedStation.getColor()));
            }
        });
    }

    private void hideSelectionLayout() {
        selectionLayoyt.setVisibility(View.INVISIBLE);

    }

    public void swap(View view){
        Station tmpst = startStation;
        startStation = endStation;
        endStation = tmpst;
        updateStartStation();
        updateEndStation();

    }

    public void openStationList(View view) {
        if(showStationList == null)
            showStationList = animationFactory.getDownToUpShowAnimation(stationsListLayout);
        view.setActivated(true);
        stationsListLayout.startAnimation(showStationList);
    }

    public void hideStationList(View view) {
        searchStation.setText("");
        if(hideStationList == null)
            hideStationList = animationFactory.getUpToDownHideAnimation(stationsListLayout);
        stationsListLayout.startAnimation(hideStationList);

        startRoute.setActivated(false);
        endRoute.setActivated(false);
    }

    public void onSelectStation(String stationId) {
        selectedStation = stationsUtil.getStationById(stationId);
        showSelectionLayout();
    }

    public void selectStartStation(View view) {
        startStation = selectedStation;
        updateStartStation();
    }

    private void updateStartStation() {
        GradientDrawable gradientDrawable = (GradientDrawable) startRouteCircle.getBackground();

        if(startStation != null) {
            startRoute.setText(startStation.getName());
            gradientDrawable.setColor(Color.parseColor(startStation.getColor()));
            startRouteButton.setVisibility(View.INVISIBLE);
            startRouteButtonDelete.setVisibility(View.VISIBLE);
        } else {
            startRoute.setText("");
            gradientDrawable.setColor(Color.parseColor("#F1F4F1"));
            startRouteButton.setVisibility(View.VISIBLE);
            startRouteButtonDelete.setVisibility(View.INVISIBLE);
        }
        hideSelectionLayout();
        buildRoute();
    }

    public void unselectStartStation(View view) {
//        webView.loadUrl("javascript:removeOverlay()");
//        webView.loadUrl("javascript:clearPath()");
//        webView.reload();
        clearPath();
        startRoute.setText("");
        startStation = null;
        startRouteButton.setVisibility(View.VISIBLE);
        startRouteButtonDelete.setVisibility(View.INVISIBLE);
    }

    public void selectEndStation(View view) {
        endStation = selectedStation;
        updateEndStation();
    }

    private void updateEndStation() {
        GradientDrawable gradientDrawable = (GradientDrawable) endRouteCircle.getBackground();
        if(endStation != null) {
            endRoute.setText(endStation.getName());
            gradientDrawable.setColor(Color.parseColor(endStation.getColor()));
            endRouteButton.setVisibility(View.INVISIBLE);
            endRouteButtonDelete.setVisibility(View.VISIBLE);
        } else {
            endRoute.setText("");
            gradientDrawable.setColor(Color.parseColor("#F1F4F1"));
            endRouteButton.setVisibility(View.VISIBLE);
            endRouteButtonDelete.setVisibility(View.INVISIBLE);
        }

        //Set color for circle
        hideSelectionLayout();
        buildRoute();
    }


    public void unselectEndStation(View view) {
        clearPath();
        endRoute.setText("");
        endStation = null;
        endRouteButton.setVisibility(View.VISIBLE);
        endRouteButtonDelete.setVisibility(View.INVISIBLE);
    }

    private void buildRoute() {
        if(startStation == null || endStation == null)
            return;
        String path = resolver.resolve(startStation.getId(), endStation.getId());
        System.out.println("PATH: " + path);
        StringBuilder stations = new StringBuilder();
        Set<String> stationsSet = new HashSet<>();
        String[] paths = path.split(";");
        for(int i = 0; i < paths.length; i++ ) {
            stationsSet.addAll(Arrays.asList(paths[i].split("_")));
        }
        for(String station : stationsSet) {
            stations.append(station);
            stations.append(";");
        }

        webView.loadUrl("javascript:buildPath(\"" + path + "\", \"" + stations.toString() + "\")");
        timingText.setText(String.valueOf((int) resolver.getTime()) + " min");
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        now.add(Calendar.MINUTE, (int) resolver.getTime());
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        arriveText.setText("Прибытие в ~" + format.format(now.getTime()));
        timingLinearLayout.setVisibility(View.VISIBLE);
    }

    private void clearPath() {
        timingText.setText("");
        arriveText.setText("");
        timingLinearLayout.setVisibility(View.INVISIBLE);
        webView.reload();
//        webView.loadUrl("javascript:clearPath()");
    }



    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if(zoomed) {
                webView.zoomOut();
                webView.zoomOut();
                webView.zoomOut();
                zoomed = false;
            }
            else {
                webView.zoomIn();
                webView.zoomIn();
                webView.zoomIn();
                zoomed = true;
            }
            return true;
        }
    }
}
