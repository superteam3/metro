package org.superteam.metro.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shubbush on 12/07/16.
 */
public class LineColorUtil {

    private Map<String, String> lineColors = new HashMap<>();

    public LineColorUtil() {
        lineColors.put("l0g","#000000");
        lineColors.put("l1g","#745C2F");
        lineColors.put("l2g","#5091BB");
        lineColors.put("l3g","#85D4F3");
        lineColors.put("l4g","#EF1E25");
        lineColors.put("l5g","#FBAA33");
        lineColors.put("l6g","#FFD803");
        lineColors.put("l7g","#B1D332");
        lineColors.put("l8g","#ACADAF");
        lineColors.put("l9g","#029A55");
        lineColors.put("l10g","#B61D8E");
        lineColors.put("l11g","#019EE0");
        lineColors.put("l12g","#0252A2");
    }

    public String getColorByLineId(String lineId) {
        return lineColors.get(lineId);
    }



}
