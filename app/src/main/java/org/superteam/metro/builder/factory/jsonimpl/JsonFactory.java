package org.superteam.metro.builder.factory.jsonimpl;

import org.superteam.metro.builder.factory.AbstractFactory;
import org.superteam.metro.builder.factory.StageSource;
import org.superteam.metro.builder.factory.StationSource;

public class JsonFactory extends AbstractFactory {

	@Override
	public StageSource createStageSource() {
		return new JsonStageSource();
	}

	@Override
	public StationSource createStantionSource() {
		return new JsonStationSource();
	}

}
