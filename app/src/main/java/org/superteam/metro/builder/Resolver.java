package org.superteam.metro.builder;

import java.util.List;

import org.jgrapht.graph.DefaultWeightedEdge;

import org.superteam.metro.builder.factory.AbstractFactory;
import org.superteam.metro.builder.factory.StageSource;
import org.superteam.metro.builder.factory.StationSource;
import org.superteam.metro.builder.factory.jsonimpl.JsonFactory;
import org.superteam.metro.builder.graphs.CustomWeightedEdge;
import org.superteam.metro.builder.graphs.PathFinder;
import org.superteam.metro.util.StationsUtil;
import org.superteam.metro.util.TransferUtil;

public class Resolver {
	
	private PathFinder pathFinder = null;

	private double time = 0.0;
	
	public Resolver() {
		AbstractFactory factory = new JsonFactory();
		StageSource stageSource = factory.createStageSource();
		
		StationSource stationSource = factory.createStantionSource();
		pathFinder = new PathFinder(stageSource, stationSource);
	}

	public String resolve(final String st1, final String st2) {
		List<CustomWeightedEdge> path = pathFinder.findPath(st1, st2);
		StringBuffer str = new StringBuffer();
        StationsUtil stationsUtil = StationsUtil.getInstance();
		TransferUtil transferUtil = TransferUtil.getInstance();
		time = 0.0;

        for(CustomWeightedEdge edge : path) {
			//str.append(edge.toString().replace(" : ", "_").replace("(", "").replace(")", "") + "|");
			time += edge.getWeight();
            String idPeregon = edge.toString()
                    .replace(" : ", "_")
                    .replace("(", "")
                    .replace(")", "");
            if(transferUtil.getTransferById(idPeregon) != null) {
                str.append(idPeregon + ";");
            }
            else {
                String[] idArrays = idPeregon.split("_");
                String idPeregonRevert = idArrays[1] + "_" + idArrays[0];
                str.append(idPeregonRevert + ";");
            }

		}
		
		//System.out.println(str.toString());
		return str.toString();
	}

	public double getTime() {
		return time;
	}
}
