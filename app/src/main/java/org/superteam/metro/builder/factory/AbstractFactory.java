package org.superteam.metro.builder.factory;

public abstract class AbstractFactory {

	public abstract StageSource createStageSource();
	
	public abstract StationSource createStantionSource();
}
