package org.superteam.metro.builder.graphs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import org.superteam.metro.builder.factory.StageSource;
import org.superteam.metro.builder.factory.StationSource;
import org.superteam.metro.model.Peregon;
import org.superteam.metro.model.Station;


public class PathFinder {
	
	private final Map<String, Station> stMap = new HashMap<String, Station>();
	
	private final WeightedGraph<Station, CustomWeightedEdge> g =
			new SimpleDirectedWeightedGraph<Station, CustomWeightedEdge>(CustomWeightedEdge.class);
	
	public PathFinder(StageSource stage, StationSource stantion) {
		if(stage == null || stantion == null)
			throw new IllegalArgumentException("");
		
		List<Station> stantions = stantion.getStationList();
		for(Station st : stantions) {
			stMap.put(st.getId(), st);
			g.addVertex(st);
		}
		
		List<Peregon> peregons = stage.getStageList();
		for(Peregon pr : peregons) {
			System.out.println(pr.getId());
			Station s1 = getStById(pr.getId().split("_")[0]);
			Station s2 = getStById(pr.getId().split("_")[1]);
			
			CustomWeightedEdge edge = g.addEdge(s1, s2);
			g.setEdgeWeight(edge, pr.getWeight() != null ? pr.getWeight() : 1L);

			CustomWeightedEdge edgeRe = g.addEdge(s2, s1);
			g.setEdgeWeight(edgeRe, pr.getWeight() != null ? pr.getWeight() : 1L);
		}
	}
	
	private Station getStById(String id) {
		return stMap.get(id);
	}
	
	public List<CustomWeightedEdge> findPath(String st1, String st2) {
		if(st1 == null || st2 == null) 
			throw new IllegalArgumentException("Неподходящее начение аргументов");
		
		Station s1 = getStById(st1);
		Station s2 = getStById(st2);
		List<CustomWeightedEdge> data = null;
		data = DijkstraShortestPath.findPathBetween(g, s1, s2);		
		return data;
	}

}
