package org.superteam.metro.builder.factory.jsonimpl.parsers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;
import org.superteam.metro.model.Peregon;

public class PeregonParser {

	public List<Peregon> readJsonStream(InputStream in) throws IOException {
		if(in == null)
			throw new IllegalArgumentException("Неверно указан источник данных");
		
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		try {
			return readPeregonArray(reader);
		} finally {
			reader.close();
		}
	}

	private List<Peregon> readPeregonArray(JsonReader reader) throws IOException {
		List<Peregon> messages = new ArrayList<Peregon>();

		reader.beginArray();
		while (reader.hasNext()) {
			messages.add(readPeregon(reader));
		}
		reader.endArray();
		return messages;
	}

	private Peregon readPeregon(JsonReader reader) throws IOException {

		String id = null;
		Long weight = null;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("id")) {
				id = reader.nextString();
			} 
			else if (name.equals("weight")) {
				weight = reader.nextLong();
			}
			else {
				reader.skipValue();
			}
		}

		reader.endObject();
		return new Peregon(id,weight);
	}
}
