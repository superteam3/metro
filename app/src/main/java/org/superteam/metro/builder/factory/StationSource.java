package org.superteam.metro.builder.factory;

import java.util.List;

import org.superteam.metro.model.Station;

public interface StationSource {

	String getPathSource();
	
	List<Station> getStationList();
	
}
