package org.superteam.metro.builder.factory.jsonimpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.superteam.metro.builder.factory.StationSource;
import org.superteam.metro.builder.factory.jsonimpl.parsers.StantionParser;
import org.superteam.metro.model.Station;

public class JsonStationSource implements StationSource {

	private static final String PATH_SOURCE = "assets/stations.json";
	
	private static final Logger logger = Logger.getLogger(JsonStationSource.class.getName());

	public String getPathSource() {
		return PATH_SOURCE;
	}

	public List<Station> getStationList() {
		List<Station> data = null;
		try {
			StantionParser stantionParser = new StantionParser();
//			InputStream is = new FileInputStream(new File(getPathSource()));
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(getPathSource());
			data = stantionParser.readJsonStream(is);
		}
		catch(IOException ioEx) {
			logger.log(Level.WARNING, ioEx.getMessage());
		}
		return data;
	}

}
