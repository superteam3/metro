package org.superteam.metro.builder.factory.jsonimpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import org.superteam.metro.builder.factory.StageSource;
import org.superteam.metro.builder.factory.jsonimpl.parsers.PeregonParser;
import org.superteam.metro.model.Peregon;

public class JsonStageSource implements StageSource {

	private static final String PATH_SOURCE = "assets/paths.json";
	
	private static final Logger logger = Logger.getLogger(JsonStageSource.class.getName());

	public String getPathToSource() {
		return PATH_SOURCE;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sergey.factory.StageSource#getStageList()
	 */
	public List<Peregon> getStageList() {
		List<Peregon> data = null;
		try {
			PeregonParser peregonParser = new PeregonParser();
//			InputStream is = new FileInputStream(new File(getPathToSource()));
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(getPathToSource());
			data = peregonParser.readJsonStream(is);
		}
		catch(IOException e) {
			logger.info("Ошибка " + e.getMessage());
		}
		return data;
	}

}
