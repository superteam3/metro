package org.superteam.metro.builder.factory;

import java.util.List;

import org.superteam.metro.model.Peregon;

public interface StageSource {
	
	public String getPathToSource();
	
	List<Peregon> getStageList();

}
