package org.superteam.metro.builder.factory.jsonimpl.parsers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;

import org.superteam.metro.builder.LineColorUtil;
import org.superteam.metro.model.Station;

/**
 * Парсер для Стнций
 * @author sergey
 *
 */
public class StantionParser {

	private LineColorUtil lineColorUtil = new LineColorUtil();

	public List<Station> readJsonStream(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		try {
			return readStantionsArray(reader);
		} finally {
			reader.close();
		}
	}

	private List<Station> readStantionsArray(JsonReader reader) throws IOException {
		List<Station> messages = new ArrayList<Station>();

		reader.beginArray();
		while (reader.hasNext()) {
			messages.add(readStantion(reader));
		}
		reader.endArray();
		return messages;
	}

	private Station readStantion(JsonReader reader) throws IOException {

		String id = null;
		String nameSt = null;
		String line = null;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("id")) {
				id = reader.nextString();
			} else if (name.equals("name")) {
				nameSt = reader.nextString();
			} else if(name.equals("line")) {
				line = reader.nextString();
			}
			else {
				reader.skipValue();
			}
		}
		reader.endObject();
		return new Station(id, nameSt, lineColorUtil.getColorByLineId(line));
	}
}
