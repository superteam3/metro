package org.superteam.metro.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Station implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName("id")
	private String id;
	
	@SerializedName("name")
	private String name;

	@SerializedName("color")
	private String color;
	
	public Station() {}
	
	public Station(String id, String name, String color) {
		this.id = id;
		this.name = name;
		this.color = color;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String namr) {
		this.name = namr;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj != this) {
			return false;
		}
		Station other = (Station) obj;
		if(this.id != other.getId()) {
			return false;
		}
		
		return true;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
	
	@Override
	public String toString() {
		return this.getId();
	}
}
