package org.superteam.metro.model;

import java.io.Serializable;

public class Peregon implements Serializable {
	
	private String id;
	
	private Long weight;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Peregon() {}
	
	public Peregon(String id) {
		this.id = id;
	}
	
	public Peregon(String id, Long weight) {
		this.id = id;
		this.weight = weight;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Long getWeight() {
		return weight;
	}

	public void setWeight(Long weight) {
		this.weight = weight;
	}

	@Override
	public boolean equals(Object obj) {
		if(this != obj) return false;
		
		Peregon other = (Peregon) obj;
		if(!this.getId().equals(other.getId()))
				return false;
		
		return true;
	}
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public String toString() {
		return this.id;
	}

}
