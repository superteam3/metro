package org.superteam.metro;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.superteam.metro.model.Station;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubbush on 14/05/16.
 */
public class StationListAdapter extends ArrayAdapter<Station> implements Filterable {

    private List<Station> originalData = null;
    private List<Station> filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public StationListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public StationListAdapter(Context context, int resource, List<Station> items) {
        super(context, resource, items);
        this.filteredData = items ;
        this.originalData = items ;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Station getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.station_list_item, null);
        }

        Station p = getItem(position);

        if (p != null) {
            TextView label = (TextView) v.findViewById(R.id.station_item_label);
            LinearLayout bullet = (LinearLayout) v.findViewById(R.id.station_item_bullet);

            if(label != null) {
                label.setText(p.getName());
            }
            if(bullet != null) {
                GradientDrawable gradientDrawable = (GradientDrawable) bullet.getBackground();
                gradientDrawable.setColor(Color.parseColor(p.getColor()));
//                p.getColor()
            }
        }

        return v;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Station> list = originalData;

            int count = list.size();
            final ArrayList<Station> nlist = new ArrayList<>(count);

            Station filterableStation;

            for (int i = 0; i < count; i++) {
                filterableStation = list.get(i);
                if (filterableStation.getName().toLowerCase().contains(filterString)) {
                    nlist.add(filterableStation);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredData = (ArrayList<Station>) results.values;
            notifyDataSetChanged();
        }

    }
}
